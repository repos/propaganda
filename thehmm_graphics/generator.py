# tool to convert texts to images. needs an SVG as a template. See line 16
# (c) roel roscam abbing 2018
# gplv3
import os,base64,sys
count = 1
svg = open(sys.argv[1]).read()  #an svg template 
text = open(sys.argv[2]).read() #a text file
magic_word = '08'

if not os.path.exists('output'):
    os.mkdir('output')

for sentence in text.split('\n'):
    files = []
    for word in sentence.split(' '):
        fn = 'output/{}.svg'.format(word)
        with open(fn, 'w') as f:
            f.write(svg.replace(magic_word, word))
        files.append(fn)
    if sentence:
        try:
            of = 'output/{}.jpg'.format(str(count).zfill(3))

            #command = "convert -delay 1 -alpha set -dispose previous {} {}".format(" ".join(files),of)
            command = "convert {} {}".format(" ".join(files),of)
            print(command)
            os.system(command)
            count+=1
        except:
            print('skipped', sentence)
            pass
    os.system('rm {}'.format(' '.join(files)))